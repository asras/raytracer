(in-package :raytracer)


(defmacro sv (obj name)
  `(slot-value ,obj ',name))
  

(defun deg-to-rad (degrees)
  (coerce (/ (* degrees 3.1415926535) 180.0) 'single-float))


(declaim (ftype (function () single-float) random-double))
(defun random-double ()
  "Return a random float in range [0, 1)."
  (random 1.0))


(declaim (ftype (function (single-float single-float) single-float) random-in-range))
(defun random-in-range (min max)
  "Return a random float in range [min, max)."
  (+ (* (random-double) (- max min)) min))


(defun clamp (value min max)
  "Clamp value to range [min, max]."
  (cond
    ((< value min) min)
    ((> value max) max)
    (t value)))


(defun outside-p (v min max)
  (or (< v min) (> v max)))
