(in-package :raytracer)

(defclass ray ()
  ((origin :initarg :origin)
   (direction :initarg :direction)))

(defmacro make-ray (&body body)
  `(make-instance 'ray ,@body))

(defun at (r s)
  "Calculate the vec3 on ray at parameter s."
  (with-slots (origin direction) r
    (vadd origin (vmult direction s))))


(defmethod print-object ((r ray) stream)
  (print-unreadable-object (r stream :type t)
    (with-slots (origin direction) r
      (let ((ox (slot-value origin 'x))
            (oy (slot-value origin 'y))
            (oz (slot-value origin 'z))
            (dx (slot-value direction 'x))
            (dy (slot-value direction 'y))
            (dz (slot-value direction 'z)))
        (format stream "~%Origin: ~A ~A ~A~%Direction: ~A ~A ~A~%" ox oy oz dx dy dz)))))



