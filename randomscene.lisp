(in-package :raytracer)


(defun make-random-scene ()
  (let ((ground-material (make-lambert (make-instance 'color :x 0.5 :y 0.5 :z 0.5)))
        (world nil))


    (loop for a from -11 to 11
          do
             (loop for b from -11 to 11
                   do
                      (let ((choose-mat (random-double))
                            (center (make-vec
                                      :x (+ a (* 0.9 (random-double)))
                                      :y 0.2
                                      :z (+ b (* 0.9 (random-double))))))
                        (if (> (len (vsub center (make-vec :x 4 :y 0.2))) 0.9)
                            (cond
                              ((< choose-mat 0.8)
                               (let* ((albedo (vmult (rand-vec3) (rand-vec3)))
                                      (sphere-mat (make-lambert albedo))
                                      (sphere (make-sphere center 0.2f0 sphere-mat)))
                                 (push sphere world)))
                              ((< choose-mat 0.95)
                               (let* ((albedo (rand-range-vec3 0.5 1.0))
                                      (fuzz (random-in-range 0.0f0 0.5f0))
                                      (sphere-mat (make-metal albedo fuzz))
                                      (sphere (make-sphere center 0.2f0 sphere-mat)))
                                 (push sphere world)))
                              (t
                               (let* (
                                      (sphere-mat (make-dielectric 1.5f0))
                                      (sphere (make-sphere center 0.2f0 sphere-mat)))
                                 (push sphere world))))
                            )
                        )
                   )
          )
    (let ((mat1 (make-dielectric 1.5f0))
          (mat2 (make-lambert (make-instance 'color :x 0.4 :y 0.2 :z 0.1)))
          (mat3 (make-metal (make-instance 'color :x 0.7 :y 0.6 :z 0.5) 0.0)))
      (push (make-sphere (make-vec :x 0.0 :y 1.0 :z 0.0) 1.0f0 mat1) world)
      (push (make-sphere (make-vec :x -4.0 :y 1.0 :z 0.0) 1.0f0 mat2) world)
      (push (make-sphere (make-vec :x 4.0 :y 1.0 :z 0.0) 1.0f0 mat3) world))
    (make-instance 'hittable-list :elements world)))
          

                              
                                   
