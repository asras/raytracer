(in-package :raytracer)


(defgeneric scatter (material in-ray hit-r attenuation-color scattered-ray)
  (:documentation "Generic function defining the scattering behaviour of a material."))

(defclass material () ())

(defclass lambertian-material (material)
  ((albedo :initarg :albedo)))

(defmacro make-lambert (albedo) `(make-instance 'lambertian-material :albedo ,albedo))

(defmethod scatter ((material lambertian-material) (in-ray ray) (hit-r hit-record)
                    (attenuation-color color) (scattered-ray ray))
  (declare (optimize (speed 3)))
  (let* ((scatter-dir (vadd (sv hit-r normal) (rand-unit-vec3))))
    (when (near-zero-p scatter-dir) (setf scatter-dir (vcopy (sv hit-r normal))))
    (setf (sv scattered-ray origin) (sv hit-r point))
    (setf (sv scattered-ray direction) scatter-dir)
    (setf (sv attenuation-color x) (sv (sv material albedo) x))
    (setf (sv attenuation-color y) (sv (sv material albedo) y))
    (setf (sv attenuation-color z) (sv (sv material albedo) z))
    t))


(defclass metal-material (material)
  ((albedo :initarg :albedo)
   (fuzz :initarg :fuzz :type real)))

(defmacro make-metal (albedo fuzz) `(make-instance 'metal-material :albedo ,albedo
                                                   :fuzz ,fuzz))

(defmethod scatter ((material metal-material) (in-ray ray) (hit-r hit-record)
                    (attenuation-color color) (scattered-ray ray))
  (declare (optimize (speed 3)))
  (let* ((reflected (reflect (normalize (sv in-ray direction)) (sv hit-r normal)))
         (new-direction (vadd reflected (vmult (rand-unit-vec3)
                                               (sv material fuzz)))))
    
    (setf (sv scattered-ray origin) (sv hit-r point))
    (setf (sv scattered-ray direction) new-direction)
    (setf (sv attenuation-color x) (sv (sv material albedo) x))
    (setf (sv attenuation-color y) (sv (sv material albedo) y))
    (setf (sv attenuation-color z) (sv (sv material albedo) z))
    (>
     (the single-float (dot (sv scattered-ray direction) (sv hit-r normal)))
     0.0)))
         

(declaim (ftype (function (dielectric-material) single-float) ir))
(defclass dielectric-material (material)
  ((index-of-refraction :initarg :index-of-refraction :reader ir :type single-float)))


(defmacro make-dielectric (index-of-refraction)
  `(make-instance 'dielectric-material :index-of-refraction ,index-of-refraction))


(declaim (ftype (function (single-float single-float) single-float) reflectance))
(defun reflectance (cos-theta refraction-ratio)
  (let* ((r (/ (- 1.0 refraction-ratio) (+ 1.0 refraction-ratio)))
         (r0 (* r r)))
    (+ r0 (* (- 1 r0) (expo (- 1 cos-theta) 5)))))


(declaim (ftype (function (single-float) single-float) fsqrt))
(defun fsqrt (x)
  (sqrt x))

(defmethod scatter ((material dielectric-material) (in-ray ray) (hit-r hit-record)
                    (attenuation-color color) (scattered-ray ray))
  (declare (optimize (speed 3)))
  (setf (sv attenuation-color x) 1.0)
  (setf (sv attenuation-color y) 1.0)
  (setf (sv attenuation-color z) 1.0)
  
  (let* ((refraction-ratio (if (sv hit-r front-face)
                               (coerce (/ 1.0 (ir material)) 'single-float)
                               (ir material)))
         (unit-dir (normalize (sv in-ray direction)))
         (dotp (dot unit-dir (sv hit-r normal)))
         (cos-theta (coerce (min (* -1.0 dotp) 1.0) 'single-float))
         (sin-theta (fsqrt (- 1.0 (* cos-theta cos-theta))))
         (cannot-refract (> (* refraction-ratio sin-theta) 1.0))
         (new-direction (if (or
                             cannot-refract
                             (> (reflectance cos-theta refraction-ratio) (random-double)))
                            (reflect unit-dir (sv hit-r normal))
                            (refract unit-dir (sv hit-r normal) refraction-ratio))))
    
    (setf (sv scattered-ray origin) (sv hit-r point))
    (setf (sv scattered-ray direction) new-direction)
    t))


