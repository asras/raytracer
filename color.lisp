(in-package :raytracer)


(defclass color (vec3) ())

(defun (setf r) (value color)
  (setf (slot-value color 'x) value))

(defun (setf g) (value color)
  (setf (slot-value color 'y) value))

(defun (setf b) (value color)
  (setf (slot-value color 'z) value))

(defmethod r ((col color))
  (slot-value col 'x))

(defmethod b ((col color))
  (slot-value col 'b))

(defmethod g ((col color))
  (slot-value col 'g))

(defun vec2col (vec)
  (with-slots (x y z) vec
    (make-instance 'color :x x :y y :z z)))
         
;; (defun write-color (stream color)
;;   "Write a color to the stream assuming the color is in range [0, 1]"
;;   (let1v color
;;     (format stream "~A ~A ~A ~%"
;;             (floor (* x 255.999))
;;             (floor (* y 255.999))
;;             (floor (* z 255.999)))))


(defun write-color (stream color number-of-samples)
  "Write a color to the stream assuming the color is in range [0, 1]"
  (with-slots (x y z) color
    (let* ((scale (/ 1.0 number-of-samples))
           (red   (* x scale))
           (green (* y scale))
           (blue  (* z scale)))
      (when (or (> red 1.0) (> green 1.0) (> blue 1.0))
        (format *error-output* "Large color: ~A~%" color))
      (format stream "~A ~A ~A ~%"
              (floor (* 256 (clamp red 0.0 0.999)))
              (floor (* 256 (clamp green 0.0 0.999)))
              (floor (* 256 (clamp blue 0.0 0.999)))))))
