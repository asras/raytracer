(in-package :raytracer)


(defclass camera ()
  (
   (aspect-ratio :initarg :aspect-ratio :initform (/ 16 9))
   (u :initarg :u :type vec3)
   (v :initarg :v :type vec3)
   (w :initarg :w :type vec3)
   (lens-radius :initarg :lensradius :type single-float)
   (viewport-height :initarg :viewport-height :initform 2)
   (viewport-width :initarg :viewport-width :initform 0)
   (focal-length :initarg :focal-length :initform 1)
   (vfov :initarg :vfov)
   (origin :initarg :origin :initform (make-vec))
   (horizontal :initarg :horizontal :initform (make-vec))
   (vertical :initarg :vertical :initform (make-vec))
   (lower-left-corner :initarg :lower-left :initform (make-vec))))

(defmethod print-object ((cam camera) stream)
  (with-slots (aspect-ratio viewport-height viewport-width focal-length origin horizontal vertical lower-left-corner) cam
    (format stream "Aspect-ratio: ~A~%Viewport height: ~A~%Viewport width: ~A~%Focal length: ~A~%Origin: ~A~%Horizontal: ~A~%Vertical: ~A~%Lower left corner: ~A~%"
    (format nil "~A~%" aspect-ratio )
    (format nil "~A~%" viewport-height )
    (format nil "~A~%" viewport-width )
    (format nil "~A~%" focal-length )
    (format nil "~A~%" origin )
    (format nil "~A~%" horizontal )
    (format nil "~A~%" vertical )
    (format nil "~A~%" lower-left-corner ))))
    

(declaim (ftype (function (vec3 vec3 vec3 integer single-float single-float) camera) std-camera))
(defun std-camera (lookfrom lookat vup deg-fov aperture focus-dist)
  "Make the standard camera."
  (let ((cam (make-instance 'camera)))
    (setf (sv cam aspect-ratio) (/ 16 9))
    (setf (sv cam vfov) deg-fov)
    (let* ((rads (coerce (* deg-fov (/ pi 180)) 'single-float))
           (h (coerce (tan (/ rads 2.0)) 'single-float)))
      (setf (sv cam viewport-height) (* 2.0 h)))
    (setf (sv cam viewport-width) (* (sv cam aspect-ratio) (sv cam viewport-height)))
    (setf (sv cam focal-length) 1)
    (setf (sv cam origin) lookfrom)
    (let* ((w (normalize (vsub lookfrom lookat)))
           (u (normalize (cross vup w)))
           (v (cross w u)))
      (setf (sv cam horizontal) (vmult u (* focus-dist (sv cam viewport-width))))
      (setf (sv cam vertical) (vmult v (* focus-dist (sv cam viewport-height))))
      (setf (sv cam lower-left-corner) (vsub-m
                                        (sv cam origin)
                                        (vmult (sv cam horizontal) 0.5)
                                        (vmult (sv cam vertical) 0.5)
                                        (vmult w focus-dist)))
      (setf (sv cam lens-radius) (/ aperture 2.0f0))
      (setf (sv cam u) u)
      (setf (sv cam w) w)
      (setf (sv cam v) v)
      
      cam)))


(defun zerovec-p (vec)
  (let1v vec
    (and (= x 0) (= y 0) (= z 0))))

(defmethod get-ray ((cam camera) (u single-float) (v single-float))
  (declare (optimize (speed 3)))
  (with-slots (origin lower-left-corner horizontal vertical) cam
    (let* ((ox (the single-float (sv origin x)))
           (oy (the single-float (sv origin y)))
           (oz (the single-float (sv origin z)))
           (lx (the single-float (sv lower-left-corner x)))
           (ly (the single-float (sv lower-left-corner y)))
           (lz (the single-float (sv lower-left-corner z)))
           (hx (the single-float (sv horizontal x)))
           (hy (the single-float (sv horizontal y)))
           (hz (the single-float (sv horizontal z)))
           (vx (the single-float (sv vertical x)))
           (vy (the single-float (sv vertical y)))
           (vz (the single-float (sv vertical z)))
           (rd (vmult (random-in-unit-disk) (sv cam lens-radius)))
           (offset (vadd
                    (vmult (sv cam u) (sv rd x))
                    (vmult (sv cam v) (sv rd y)))))
      (make-ray :origin (vadd (sv cam origin) offset)
        :direction (make-vec
                     :x (-
                         (+ lx
                            (* hx u)
                            (* vx v))
                         ox
                         (the single-float (sv offset x)))
                     :y (-
                         (+ ly
                            (* hy u)
                            (* vy v))
                         oy
                         (the single-float (sv offset y)))
                     :z (-
                         (+ lz
                            (* hz u)
                            (* vz v))
                         oz
                         (the single-float (sv offset z))))))))
