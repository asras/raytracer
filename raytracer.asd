;;;; raytracer.asd

(asdf:defsystem #:raytracer
  :description "A basic raytracer"
  :author "Asbjørn Rasmussen asras28@gmail.com"
  :license  "MIT License"
  :version "0.0.1"
  :serial t
  :depends-on (#:sb-sprof)
  :components ((:file "package")
               (:file "timing")
               (:file "utils")
               (:file "writeimage")
               (:file "linalg")
               (:file "color")
               (:file "ray")
               (:file "hittable")
               (:file "camera")
               (:file "materials")
               (:file "randomscene")
               (:file "raytracer")))
