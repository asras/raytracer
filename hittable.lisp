(in-package :raytracer)

(defgeneric hit (smth ray tmin tmax)
  (:documentation "Generic function that determines whether a ray hits 'something'. Should return a hit-record if the something was hit."))

(declaim (ftype (function (hit-record) single-float) get-hitp))
(defclass hit-record ()
  ((point :initarg :point :initform (make-vec))
   (normal :initarg :normal :initform (make-vec))
   (hit-param :initarg :hit-param :initform 0.0f0 :type single-float :accessor get-hitp)
   (front-face :initarg :front-face :initform nil)
   (was-hit :initarg :was-hit :initform nil)
   (material :initarg :material)))

(defmethod print-object ((hit-r hit-record) stream)
  (print-unreadable-object (hit-r stream :type t)
    (with-slots (point normal hit-param was-hit) hit-r
      (format stream
              "~%Point: ~A~%Normal: ~A~%Hit-param: ~A~%Was-hit: ~A~%"
              (format nil "~A" point) (format nil "~A" normal) (format nil "~A" hit-param) (format nil "~A" was-hit)))))
              

(declaim (ftype (function (hit-record ray vec3) null) set-face-normal))
(defmethod set-face-normal ((hit-r hit-record) (r ray) (outward-normal vec3))
  (with-slots (direction) r
    (let ((front (< (dot direction outward-normal) 0.0)))
      (setf (sv hit-r front-face) front)
      (if front
          (setf (slot-value hit-r 'normal) outward-normal)
          (setf (slot-value hit-r 'normal) (vmult outward-normal -1)))
      nil)))
          
      

(defmacro make-hit (&body body)
  `(make-instance 'hit-record ,@body))


(declaim (ftype (function (sphere) single-float) srad))
(defclass sphere ()
  ((center :initarg :center :type vec3)
   (radius :initarg :radius :type single-float :accessor srad)
   (material :initarg :material)))

(defmacro make-sphere (center radius material)
  `(make-instance 'sphere :center ,center :radius ,radius :material ,material))

(defmethod print-object ((s sphere) stream)
  (print-unreadable-object (s stream :type t)
    (with-slots (center radius) s
      (with-slots (x y z) center
        (format stream "~%Center: ~A ~A ~A~%Radius: ~A~%" x y z radius)))))


(declaim (ftype (function (single-float ray vec3 single-float material) hit-record) make-sphere-hit))
(defun make-sphere-hit (hit-param r center radius material)
  (declare (optimize (speed 3)))
  (let* ((point (at r hit-param))
         (normal (vdiv (vsub point center) radius))
         (hit-r (make-hit
                  :point point
                  :normal normal
                  :hit-param hit-param :was-hit t
                  :material material)))
    (set-face-normal hit-r r normal)
    hit-r))


(defmethod hit ((s sphere) (r ray) (tmin single-float) (tmax single-float))
  (declare (optimize (speed 3)))
  (let ((center (sv s center))
        (radius (srad s))
        (origin (sv r origin))
        (direction (sv r direction)))
    (let* ((oc (vsub origin center))
           (a      (len-sqr direction))
           (half-b (dot oc direction))
           (c (- (len-sqr oc) (* radius radius)))
           (disc (- (* half-b half-b) (* a c))))
      (if (< disc 0.0f0)
          (make-hit) ; no hit
          
          (let* ((sqrtd (coerce (sqrt disc) 'single-float)) 
                 (root-1 (/ (- (- half-b) sqrtd) a))
                 (root-2 (/ (+ (- half-b) sqrtd) a)))
            (if (and
                 (or (< root-1 tmin) (< tmax root-1))
                 (or (< root-2 tmin) (< tmax root-2)))
                (make-hit) ; no hit
                
                (if (and (> root-1 tmin) (> tmax root-1))
                    (make-sphere-hit root-1 r center radius (sv s material))
                    (make-sphere-hit root-2 r center radius (sv s material)))))))))


(defclass hittable-list ()
  ((elements :initarg :elements :initform nil)))

(defmethod print-object ((ls hittable-list) stream)
  (loop for el in (sv ls elements)
        do (print-object el stream)))

(defun make-hit-list (&rest body)
  (make-instance 'hittable-list :elements body))


(defmethod hit ((ls hittable-list) (r ray) (tmin single-float) (tmax single-float))
  (declare (optimize (speed 3)))
  (let ((els (sv ls elements))
        (best-hit (make-hit))
        (tbest tmax))
    (loop for hittable in els
          for hit-r = (hit hittable r tmin tmax)
          do
             (let ((hit-par (the single-float (sv hit-r hit-param))))
               (if (and (sv hit-r was-hit) (< hit-par tbest))
                   (progn
                     (setf tbest hit-par)
                     (setf best-hit hit-r)))))
    best-hit))
