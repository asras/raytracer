(in-package :raytracer)


(defparameter *timing-data* (make-hash-table))

(defmacro with-gensyms ((&rest names) &body body)
  `(let ,(loop for n in names collect `(,n (gensym)))
     ,@body))

;; (defmacro with-timing (label priority &body body)
;;   (with-gensyms (start)
;;     `(let ((,start (get-internal-run-time)))
;;        (unwind-protect (progn ,@body)
;;          (push (list ',label ,start (get-internal-run-time)) *timing-data*)))))

(defmacro with-timing (label &body body)
  (let ((start (gensym))
        (end (gensym)))
    `(let ((,start (get-internal-run-time)))
       (unwind-protect (progn ,@body)
         (let ((,end (get-internal-run-time)))
           (incf (gethash ',label *timing-data* 0) (- ,end ,start)))))))


(defun clear-timing-data ()
  (setf *timing-data* (make-hash-table)))


(defun compare-priority (a b)
  (< (second a) (second b)))

(defun show-timing-data ()
    (loop for (label time %-of-total) in (compile-timing-data) do
      (format t "~3d% ~a: ~d ticks~%"
              %-of-total label time)))

(defun compile-timing-data ()
  (let ((total-time (loop for label being the hash-keys in *timing-data*
                          summing (gethash label *timing-data*) into total
                          finally (return total))))
    (sort
         (loop for label being the hash-keys in *timing-data*
                    collect
                    (let ((time (gethash label *timing-data*)))
                      (list label time (round (* 100 (/ time total-time))))))
              #'> :key #'third)))
     
        
           
  
  ;; (loop with timing-table = (make-hash-table)
  ;;       with count-table = (make-hash-table)
  ;;       for (label start end) in *timing-data*
  ;;       for time = (- end start)
  ;;       summing time into total
  ;;       do
  ;;          (incf (gethash label timing-table 0) time)
  ;;          (incf (gethash label count-table 0) time)
  ;;       finally
  ;;          (return
  ;;            (sort
  ;;             (loop for label being the hash-keys in timing-table
  ;;                   collect
  ;;                   (let ((time (gethash label timing-table))
  ;;                         (count (gethash label count-table)))
  ;;                     (list label time count (round (/ time count)) (round (* 100 (/ time total))))))
  ;;             #'> :key #'fifth))))
