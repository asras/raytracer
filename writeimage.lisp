; (in-package #:raytracer)


(defmacro formatm (stream &rest args)
  (cons 'progn (loop for arg in args
        collect `(format ,stream ,arg))))

(defun write-to-stream (stream)
  (let ((image-width 256)
        (image-height 256))

    (format stream "P3~%")
    (format stream "~{~a ~}" (list image-width image-height))
    (format stream "~%255~%")

    (loop for j from (- image-height 1) downto 0
          do
             (format *error-output* "Scanlines remaining: ~A ~%" j)
             (loop for i below image-width
                   do
                      (let* ((r (/ i (- image-width 1)))
                             (g (/ j (- image-height 1)))
                             (b 0.25)
                             (col (make-instance 'color
                                                 :x r
                                                 :y g
                                                 :z b))
                             )
                        (write-color stream col)))))
  (format *error-output* "Done~%"))
                        

(defun test-to-file (filename)
  (with-open-file (stream filename :direction :output :if-exists :overwrite
                          :if-does-not-exist :create)
    (write-to-stream stream)))


