(in-package :raytracer)

(defclass vec3 ()
  ((x :initarg :x
      :initform 0 :type single-float)
   (y :initarg :y
      :initform 0 :type single-float)
   (z :initarg :z
      :initform 0 :type single-float)))

(defmacro make-vec (&body body)
  `(make-instance 'vec3 ,@body))
  

(defmacro let2v (v1 v2 &body body)
  "Let-declare the components of two vec3s as x1, x2, etc."
  `(let ((x1 (slot-value ,v1 'x))
         (y1 (slot-value ,v1 'y))
         (z1 (slot-value ,v1 'z))
         (x2 (slot-value ,v2 'x))
         (y2 (slot-value ,v2 'y))
         (z2 (slot-value ,v2 'z)))
     ,@body))

(defmacro let1v (v1 &body body)
  "Let-declare the components of one vec3 as x, y, z."
  `(let ((x (slot-value ,v1 'x))
         (y (slot-value ,v1 'y))
         (z (slot-value ,v1 'z)))
    ,@body))

(defmethod print-object ((v vec3) stream)
  (print-unreadable-object (v stream :type t)
    (with-slots (x y z) v
      (format stream ": ~A ~A ~A" x y z))))


;; ADDITION
(defgeneric vadd (vec-one vec-two)
  (:documentation "Add two vectors"))

(defmethod vadd ((v1 vec3) (v2 vec3))
  (let2v v1 v2
    (make-instance 'vec3 :x (+ x1 x2)
                         :y (+ y1 y2)
                         :z (+ z1 z2))))

(defmethod vadd ((v vec3) (s number))
  (let ((x1 (slot-value v 'x))
        (y1 (slot-value v 'y))
        (z1 (slot-value v 'z)))
    (make-instance 'vec3 :x (+ x1 s)
                         :y (+ y1 s)
                         :z (+ z1 s))))

(defun vadd-m (&rest vectors)
  (when vectors
    (let ((value (vmult (car vectors) 1.0))
          (remaining (cdr vectors)))
      (loop for vec in remaining
            do (vadd! value vec))
      value)))
  

(defgeneric vadd! (vec-one vec-two)
  (:documentation "Add two vectors by modifying the first"))

(defmethod vadd! ((v1 vec3) (v2 vec3))
  (let ((x1 (slot-value v1 'x))
        (y1 (slot-value v1 'y))
        (z1 (slot-value v1 'z))
        (x2 (slot-value v2 'x))
        (y2 (slot-value v2 'y))
        (z2 (slot-value v2 'z)))
    (setf (slot-value v1 'x) (+ x1 x2))
    (setf (slot-value v1 'y) (+ y1 y2))
    (setf (slot-value v1 'z) (+ z1 z2)))
  v1)

;; (defmethod vadd! ((v1 vec3) (v2 vec3))
;;   (let ((x1 (slot-value v1 'x))
;;         (y1 (slot-value v1 'y))
;;         (z1 (slot-value v1 'z))
;;         (x2 (slot-value v2 'x))
;;         (y2 (slot-value v2 'y))
;;         (z2 (slot-value v2 'z)))
;;     (setf (slot-value v1 'x) (+ (slot-value v1 'x) (slot-value v2 'x)))
;;     (setf (slot-value v1 'y) (+ (slot-value v1 'y) (slot-value v2 'y)))
;;     (setf (slot-value v1 'z) (+ (slot-value v1 'z) (slot-value v2 'z)))))


(defmethod vsub ((v1 vec3) (v2 vec3))
  (let2v v1 v2
    (make-instance 'vec3 :x (- x1 x2)
                         :y (- y1 y2)
                         :z (- z1 z2))))

;; SUBTRACT
(defgeneric vsub (vec-one vec-two)
  (:documentation "Sub two vectors"))


(defmethod vsub ((v vec3) (s number))
  (let ((x1 (slot-value v 'x))
        (y1 (slot-value v 'y))
        (z1 (slot-value v 'z)))
    (make-instance 'vec3 :x (- x1 s)
                         :y (- y1 s)
                         :z (- z1 s))))

(defun vsub-m (&rest vectors)
  "Subtract many vectors."
  (when vectors
    (let ((value (vmult (car vectors) 1.0))
          (remaining (cdr vectors)))
      (loop for vec in remaining
            do (vsub! value vec))
      value)))

(defgeneric vsub! (vec-one vec-two)
  (:documentation "Sub two vectors by modifying the first"))

(defmethod vsub! ((v1 vec3) (v2 vec3))
  (let ((x1 (slot-value v1 'x))
        (y1 (slot-value v1 'y))
        (z1 (slot-value v1 'z))
        (x2 (slot-value v2 'x))
        (y2 (slot-value v2 'y))
        (z2 (slot-value v2 'z)))
    (setf (slot-value v1 'x) (- x1 x2))
    (setf (slot-value v1 'y) (- y1 y2))
    (setf (slot-value v1 'z) (- z1 z2))))



;; MULTIPLY
(defgeneric vmult (v1 v2)
  (:documentation "Multiplication for vectors"))

(defmethod vmult ((v vec3) (s number))
  (let ((x (slot-value v 'x))
        (y (slot-value v 'y))
        (z (slot-value v 'z)))
    (make-instance 'vec3 :x (* x s) :y (* y s) :z (* z s))))

(defmethod vmult ((v1 vec3) (v2 vec3))
  (let ((x1 (slot-value v1 'x))
        (y1 (slot-value v1 'y))
        (z1 (slot-value v1 'z))
        (x2 (slot-value v2 'x))
        (y2 (slot-value v2 'y))
        (z2 (slot-value v2 'z)))
    (make-instance 'vec3 :x (* x1 x2)
                         :y (* y1 y2)
                         :z (* z1 z2))))


(defgeneric vmult! (v1 v2)
  (:documentation "Multiplication for vectors by modifying the first argument"))

(defmethod vmult! ((v1 vec3) (s number))
  (let ((x1 (slot-value v1 'x))
        (y1 (slot-value v1 'y))
        (z1 (slot-value v1 'z)))
    (setf (slot-value v1 'x) (* x1 s))
    (setf (slot-value v1 'y) (* y1 s))
    (setf (slot-value v1 'z) (* z1 s))
    v1))



(defmethod vmult! ((v1 vec3) (v2 vec3))
  (let ((x1 (slot-value v1 'x))
        (y1 (slot-value v1 'y))
        (z1 (slot-value v1 'z))
        (x2 (slot-value v2 'x))
        (y2 (slot-value v2 'y))
        (z2 (slot-value v2 'z)))
    (setf (slot-value v1 'x) (* x1 x2))
    (setf (slot-value v1 'y) (* y1 y2))
    (setf (slot-value v1 'z) (* z1 z2))
    v1))



(defgeneric vdiv (v1 s)
  (:documentation "Division for vectors"))

(defmethod vdiv ((v1 vec3) (s number))
  (let ((x (slot-value v1 'x))
        (y (slot-value v1 'y))
        (z (slot-value v1 'z)))
    (make-instance 'vec3 :x (coerce (/ x s) 'single-float)
                         :y (coerce (/ y s) 'single-float)
                         :z (coerce (/ z s) 'single-float))))


(defgeneric vdiv! (v1 s)
  (:documentation "Division for vectors by modifying the first argument"))

(defmethod vdiv! ((v1 vec3) (s number))
  (let ((x (slot-value v1 'x))
        (y (slot-value v1 'y))
        (z (slot-value v1 'z)))
    (setf (slot-value v1 'x) (/ x s))
    (setf (slot-value v1 'y) (/ y s))
    (setf (slot-value v1 'z) (/ z s))))




(defgeneric len-sqr (v)
  (:documentation "Return the length squared of the vector"))

(declaim (ftype (function (vec3) single-float) len-sqr))
(defmethod len-sqr ((v vec3))
  (with-slots (x y z) v
    (+ (* x x) (* y y) (* z z))))
  ;; (reduce #'+ (mapcar #'(lambda (x) (* x x)) (list (slot-value v 'x)
  ;;                                                  (slot-value v 'y)
  ;;                                                  (slot-value v 'z)))))


(defgeneric len (v)
  (:documentation "Return the length of the vector"))

(defmethod len ((v vec3))
  (fsqrt (len-sqr v)))


(defgeneric dot (v1 v2)
  (:documentation "Dot product"))


(declaim (ftype (function (vec3 vec3) single-float) dot))
(defmethod dot ((v1 vec3) (v2 vec3))
  (let2v v1 v2
    (+ (* x1 x2) (* y1 y2) (* z1 z2))))

(defgeneric cross (v1 v2)
  (:documentation "Cross product"))

(defmethod cross ((v1 vec3) (v2 vec3))
  (let2v v1 v2
    (make-instance 'vec3 :x (- (* y1 z2) (* z1 y2))
                         :y (- (* z1 x2) (* x1 z2))
                         :z (- (* x1 y2) (* y1 x2)))))

(defgeneric normalize (v)
  (:documentation "Return a normalized version"))

(defmethod normalize ((v vec3))
  (vdiv v (len v)))

(defgeneric normalize! (v)
  (:documentation "Return a normalized version"))

(defmethod normalize! ((v vec3))
  (let1v v
    (let ((norm (len v)))
      (setf (slot-value v 'x) (/ x norm))
      (setf (slot-value v 'y) (/ y norm))
      (setf (slot-value v 'z) (/ z norm))
      v)))


(defun replace-sym (src dst sym-list)
  (mapcar #'(lambda (x) (if (eq src x)
                            dst
                            (if (consp x)
                                (replace-sym x src dst)
                                x)))
                            sym-list))
(defmacro curry (fn &rest args)
  `#'(lambda (x) (funcall ,fn ,@args x)))

(defmacro compose (fn1 fn2)
  `#'(lambda (x) (funcall ,fn1 (funcall ,fn2 x))))


(defclass point (vec3) ())


(defun rand-vec3 ()
  "Return a vec3 with components randomly selected from [0, 1)."
  (make-vec :x (random-double) :y (random-double) :z (random-double)))

(defun rand-range-vec3 (min max)
  "Return a vec3 with components randomly selected from [min, max]."
  (make-vec
    :x (random-in-range min max)
    :y (random-in-range min max)
    :z (random-in-range min max)))


(defmacro make-polar-vec (rad phi theta)
  `(let ((x (* ,rad (sin ,theta) (cos ,phi)))
        (y (* ,rad (sin ,theta) (sin ,phi)))
        (z (* ,rad (cos ,theta))))
    (make-vec :x x :y y :z z)))

(defun rand-sphere-vec3 ()
  "Return a vector randomly selected from the unit sphere."
  (let ((rad (random-in-range 0.0 1.0))
        (phi (random-in-range 0.0 (* 2.0 3.1415926535)))
        (theta (random-in-range 0.0 3.14159265358979)))
    (make-polar-vec rad phi theta)))


(defun rand-unit-vec3 ()
  (let ((vec (rand-sphere-vec3)))
    (loop for i below 10
          while (< (len-sqr vec) 0.001)
          do (setf vec (rand-sphere-vec3)))
    (normalize vec)))

(defun random-in-hemisphere (normal)
  (let ((vec (rand-sphere-vec3)))
    (if (> (dot vec normal) 0.0)
        vec
        (vmult vec -1.0))))


(defun near-zero-p (vec)
  (let ((eps 1e-8))
    (with-slots (x y z) vec
      (and (< (abs x) eps) (< (abs y) eps) (< (abs z) eps)))))


(defun vcopy (vec)
  (with-slots (x y z) vec
    (make-vec :x x :y y :z z)))


(defun reflect (vec normal)
  (vsub vec (vmult normal (* 2 (dot vec normal)))))

(defun refract (uv normal eta_i-over-eta_t)
  (let* ((cos-theta (min (* -1.0 (dot uv normal)) 1.0))
         (r-out-perp (vmult (vadd uv (vmult normal cos-theta)) eta_i-over-eta_t))
         (r-out-parallel (vmult
                          normal
                          (* -1.0 (sqrt (abs (- 1.0 (len-sqr r-out-perp))))))))
    (vadd r-out-perp r-out-parallel)))
         
(defun random-in-unit-disk ()
  (let ((v (make-vec :x (random-in-range -1.0f0 1.0f0) :y (random-in-range -1.0f0 1.0f0) :z 0.0)))
    (loop while (>= (len-sqr v) 1.0)
          do (setf (sv v x) (random-in-range -1.0f0 1.0f0))
             (setf (sv v y) (random-in-range -1.0f0 1.0f0)))
    v))
