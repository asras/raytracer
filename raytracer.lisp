;;;; raytracer.lisp
(in-package #:raytracer)



(defun make-background-color (r)
  (with-slots (origin direction) r
    (let* ((unit-dir (normalize direction))
           (s (* (+ (slot-value unit-dir 'y) 1.0) 0.5)))
      (vec2col
        (vadd
         (vmult (make-instance 'vec3 :x 1.0 :y 1.0 :z 1.0) (- 1.0 s))
         (vmult (make-instance 'vec3 :x 0.5 :y 0.7 :z 1.0) s))))))


(defun expo (a b)
  (do ((x 1 (* x a))
       (y 0 (+ y 1)))
      ((= y b) x)))


(defmacro got-hit (hit-r)
  `(sv ,hit-r was-hit))
        

(defun ray-color (r hittable-obj rec-depth final-color hit-r)
  (setf (sv final-color x) 1.0f0)
  (setf (sv final-color y) 1.0f0)
  (setf (sv final-color z) 1.0f0)
  (let ((scattered (make-ray :origin (make-vec) :direction (make-vec)))
        (att (make-instance 'color)))
        
    (loop named outer
          for i below rec-depth
          do
             (setf hit-r (hit hittable-obj r 0.001f0 10000.0f0))
             ;; (setf hit-r (with-timing hit (hit hittable-obj r 0.001f0 10000.0f0)))
             (if (got-hit hit-r)
                 ;; Something got hit
                 (let ((scattered-p
                         (scatter (sv hit-r material) r hit-r att scattered)
                         ))
                   (when (< (len-sqr att) 0.00001) (error "No color"))
                   (setf r scattered)
                   (if scattered-p
                       ;; Scattering happened: compound attenuation
                       (vmult! final-color att)
                       ;; Absorption happened: set color to zero and return
                       (progn
                         (vmult! final-color 0.0)
                         ;; (when (> i 5) (format *error-output* "~A~%" i))
                         (return-from outer))))
                   
                 ;; Nothing got hit: multiply attenuation (which is in final-color) by background color to get final color
                 ;; Then return from loop
                 (let* ((unit-dir (normalize (sv r direction)))
                        (s1 (* 0.5 (+ 1.0 (sv unit-dir y))))
                        (s2 (- 1.0 s1)))
                   (vmult! final-color
                           (vadd (vmult (make-instance 'color :x 1 :y 1 :z 1) s2)
                                 (vmult (make-instance 'color :x 0.5 :y 0.7 :z 1.0) s1)))
                   (return-from outer))))
    final-color))



(defparameter *aspect-ratio* (coerce (/ 16.0 9.0) 'single-float))
(defparameter *image-width* 400)
;; (defparameter *image-height* (floor (/ *image-width* *aspect-ratio*)))
(defparameter *image-height* 225)

(defparameter *viewport-height* 2.0f0)
(defparameter *viewport-width* (* *aspect-ratio* *viewport-height*))
(defparameter *focal-length* 1.0f0)

(defvar *origin* (make-instance 'vec3 :x 0f0 :y 0f0 :z 0f0))
(defparameter *horizontal* (make-instance 'vec3 :x *viewport-width*))
(defparameter *vertical* (make-instance 'vec3 :y *viewport-height*))
(defparameter *lower-left-corner* (vsub *origin*
                                        (vadd
                                         (vadd (vdiv *horizontal* 2) (vdiv *vertical* 2))
                                         (make-instance 'vec3 :z *focal-length*))))


;; (defparameter *albedo* (make-instance 'color :x 0.75 :y 0.75 :z 0.75))
(defparameter *albedo* (make-instance 'color :x 0.65f0 :y 0.65f0 :z 0.65f0))

(defparameter *material-ground* (make-lambert (make-instance 'color :x 0.8f0 :y 0.8f0 :z 0.0f0)))
(defparameter *material-center* (make-lambert (make-instance 'color :x 0.1f0 :y 0.2f0 :z 0.5f0)))
;; (defparameter *material-left* (make-metal (make-instance 'color :x 0.8f0 :y 0.8f0 :z 0.8f0) 0.3))
;; (defparameter *material-center* (make-dielectric 1.5))
(defparameter *material-left* (make-dielectric 1.5))

;; (defparameter *material-right* (make-metal (make-instance 'color :x 0.8f0 :y 0.6f0 :z 0.2f0) 0.0))
(defparameter *material-right* (make-metal (make-instance 'color :x 0.95f0 :y 0.95f0 :z 0.95f0) 0.0))


;; (defparameter *world* (make-random-scene))
;; (defparameter *world* (make-hit-list
;;                        (make-instance 'marching-sphere
;;                                       :center (make-vec :x 0.0 :y 1.0 :z 0.0)
;;                                       :radius 1.0f0
;;                                       :material (make-lambert (make-instance 'color :x 0.2 :y 0.2 :z 0.8)))
;;                        (make-instance 'marching-sphere
;;                                       :center (make-vec :x 0.0 :y 0.0 :z 0.0)
;;                                       :radius 1.0f0
;;                                       :material (make-dielectric 1.5))))
  

(defparameter *world* (make-hit-list
                       (make-sphere (make-vec :y -100.5f0 :z -1.0f0) 100.0f0 *material-ground*)
                       (make-sphere (make-vec :z -1.0f0) 0.5f0 *material-center*)
                       (make-sphere (make-vec :x -1.0f0 :z -1.0f0) 0.5f0 *material-left*)
                       (make-sphere (make-vec :x -1.0f0 :z -1.0f0) -0.45f0 *material-left*)
                       (make-sphere (make-vec :x 1.25f0 :z -1.0f0) 0.5f0 *material-right*)
                         
))



(defparameter *samples-per-pixel* 200)
;; (defparameter lookfrom (make-vec :x 13.0 :y 2.0 :z 3.0))
(defparameter lookfrom (make-vec :x 3.0 :y 3.0 :z 3.0))
(defparameter lookat (make-vec :x 0.0 :y 0.0 :z 0.0))
(defparameter vup (make-vec :x 0.0 :y 1.0 :z 0.0))
(defparameter *camera* (std-camera
                        lookfrom lookat vup 20 0.1f0 10.0f0))
;; (defparameter *camera* (std-camera
;;                         (make-vec) (make-vec :z -1) (make-vec :y 1) 90))
(defparameter *max-depth* 100)


(defun zero-out (vec)
  (setf (sv vec x) 0)
  (setf (sv vec y) 0)
  (setf (sv vec z) 0))


(defun trace-to-stream (stream)
  (format stream "P3~%")
  (format stream "~{~a ~}" (list *image-width* *image-height*))
  (format stream "~%255~%")
  (let* ((pixel-color (make-instance 'color))
         (trace-col   (make-instance 'color))
         (hit-r (make-hit))
         (i 0.0f0)
         (j (- *image-height* 1.0))
         (u 0.0f0)
         (v 0.0f0)
         (r (make-ray))
         (ray-col (make-instance 'color)))
    (dotimes (ij *image-height*)
      (format *error-output* "Scanlines remaining: ~A~%" j)
      (setf i 0.0f0)
      (dotimes (ii *image-width*)
        (zero-out pixel-color)
        (dotimes (s *samples-per-pixel*)
          (setf u (/ (+ i (random-double)) *image-width*))
          (setf v (/ (+ j (random-double)) *image-height*))
          (setf r (get-ray *camera* u v))
          (setf ray-col (ray-color r *world* *max-depth* trace-col hit-r))
          (vadd! pixel-color ray-col)
          )
        (incf i)
        (write-color stream pixel-color *samples-per-pixel*))
      (decf j)))
  (format *error-output* "Done~%"))
  
 

(defun trace-to-file (filename)
  (with-open-file (stream filename :direction :output :if-exists :overwrite
                                   :if-does-not-exist :create)
    (trace-to-stream stream)))

(defun trace-to-file-alloc (filename)
  (with-open-file (stream filename :direction :output :if-exists :overwrite
                                   :if-does-not-exist :create)
    (sb-sprof:with-profiling (:mode :alloc :loop nil :show-progress t :max-samples 100 :report :flat)
        (trace-to-stream stream))))


(defun trace-to-file-time (filename)
  (with-open-file (stream filename :direction :output :if-exists :overwrite
                                   :if-does-not-exist :create)
    (sb-sprof:with-profiling (:mode :time :loop nil :show-progress t :max-samples 100 :report :flat)
        (trace-to-stream stream))))


(defun main ()
  (trace-to-file "trace.ppm"))
