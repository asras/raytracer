



(defparameter v (make-instance 'vec3 :x 0.0 :y 0.0 :z 0.0))

(defparameter orig (make-vec))
(defparameter dir (normalize! (make-instance 'vec3 :z -1)))

(defparameter r (make-instance 'ray :origin orig :direction dir))

(defparameter test-sphere (make-instance 'sphere :center (make-vec :z -5) :radius 2.0))
(defparameter test-sphere-2 (make-instance 'sphere :center (make-vec :z -10) :radius 2.0))
(defparameter test-sphere-3 (make-instance 'sphere :center (make-vec :z -2.5) :radius 1.0))

(defparameter hit-list (make-hit-list test-sphere test-sphere-2 test-sphere-3))


(defmacro tv (expr) `(format t "x: ~A~%y: ~A~%z: ~A~%" (type-of (sv ,expr x)) (type-of (sv ,expr y)) (type-of (sv ,expr z))))


(defmacro tt (expr) `(type-of ,expr))

