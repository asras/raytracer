;;;; package.lisp

(defpackage #:raytracer
  (:use #:cl)
  (:use #:sb-sprof))


(in-package :raytracer)

(declaim (optimize (speed 3)))
